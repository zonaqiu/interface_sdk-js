/**
 * the ut for union type
 */
export type Capability = 'retrieve' | 'touchGuide' | 'keyEventObserver';