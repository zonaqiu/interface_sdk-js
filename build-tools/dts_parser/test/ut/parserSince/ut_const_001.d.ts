/**
 * the ut for const in namespace, has the tag of constant but no initializer
 */
export namespace test {
  /**
   * @constant
   */
  const name: string
}