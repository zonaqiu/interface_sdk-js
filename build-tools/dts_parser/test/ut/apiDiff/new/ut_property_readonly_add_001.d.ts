/**
 * the ut for property about add one which is readonly
 *
 */
export interface Test {
  readonly constant: 1;
  readonly constant2: 2;
}
