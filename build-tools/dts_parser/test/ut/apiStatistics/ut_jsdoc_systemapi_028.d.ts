/**
 * the ut for jsdoc about systemapi
 *
 * @systemapi
 */
export class Test {
  /**
   * 
   *
   * @param {string} str
   */
  func(str: string): void;
}
