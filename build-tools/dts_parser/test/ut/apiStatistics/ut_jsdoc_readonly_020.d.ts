/**
 * the ut for jsdoc about readonly
 *
 */
export interface Test {
  /**
   * @readonly
   */
  readonly str: string;
}
