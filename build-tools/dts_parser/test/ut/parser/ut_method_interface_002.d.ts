/**
 * the ut for method in interface
 *
 */
export interface Test {
  (str: string): void;
}
