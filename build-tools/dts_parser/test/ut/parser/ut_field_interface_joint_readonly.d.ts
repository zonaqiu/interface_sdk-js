declare namespace testNamespace {
  interface TestInterface {
    readonly type: string | number | boolean;
  }
}
