/**
 * the ut for method in interface
 *
 */
export interface Test {
  new (str: string): void;
  new (str: string, str2: string): void;
}
