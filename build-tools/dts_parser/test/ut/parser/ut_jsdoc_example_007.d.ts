/**
 * the ut for jsdoc about example
 * 
 */
export interface Test {
  /**
   * @example : Set(ImageVideoKey.TITLE, "newTitle"), call commitModify after set value
   */
  set(member: string, value: string): void;
}
