/**
 * the ut for jsdoc about enum
 *
 * @enum { number } the test enum description
 */
export enum test {
  ONE = 1,
  TWO = 2,
  THREE = 3,
}
